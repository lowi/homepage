import '../sass/index.sass'

// import wasserfarben from './wasserfarben'
import stippling from './stippling'
import μ from './util'

function init() {
  console.log('Ready!')
  // let w = new wasserfarben.init('img/turner.jpg', 'img/mask.png', {})
  stippling.init('img/gradient.png', {})

}

μ.ready(init)

