function addEvent(object, type, callback) {
  if (object == null || typeof(object) == 'undefined') return
  if (object.addEventListener) object.addEventListener(type, callback, false)
  else if (object.attachEvent) object.attachEvent('on' + type, callback)
  else object['on'+type] = callback }


function ready(fn) {
  if (document.attachEvent ? document.readyState === 'complete' : document.readyState !== 'loading') fn()
  else addEvent(document, 'DOMContentLoaded', fn)}


function startAnimation(fps, fn) {
  let fpsInterval   = 1000 / fps, 
      then          = Date.now() + 2000,
      startTime     = then, now, elapsed,
      animate     = () => {
                      requestAnimationFrame(animate)
                      now = Date.now()
                      elapsed = now - then
                      if (elapsed > fpsInterval) {
                        then = now - (elapsed % fpsInterval)
                        fn() }}
  animate() }

function clearElement(element) {
  while (element.hasChildNodes()) element.removeChild(element.lastChild)
}

function scrollTop() {
  let doc = document.documentElement
      // left = (window.pageXOffset || doc.scrollLeft) - (doc.clientLeft || 0);
  return (window.pageYOffset || doc.scrollTop)  - (doc.clientTop || 0) }

function guid() {
  let s4 = () =>  Math.floor((1 + Math.random()) * 0x10000)
                    .toString(16)
                    .substring(1)
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4() }

function fontSize(ε) {
  let style = window.getComputedStyle(ε, null).getPropertyValue('font-size')
  return parseFloat(style)
}


function getDevice(width) {
  let breakpoints = { 480:  'mobile',
                      769:  'tablet',
                      1000: 'desktop',
                      1192: 'widescreen'},
      device      = _.find(breakpoints, (_, k) => width < parseInt(k) )
  return device || 'fullhd' }

// function isTouchDevice() { 
//   console.log('isMobile', isMobile)
//   return isMobile.phone || isMobile.seven_inch || isMobile.tablet }

// hleper function to detect whether or not the browser is touchy
function isTouchDevice() {
  return 'ontouchstart' in window // works on most browsers 
      || navigator.maxTouchPoints } // works on IE10/11 and Surface

let aminationEasings = ['linear', 'easeInQuad', 'easeInCubic', 'easeInQuart', 'easeInQuint', 'easeInSine', 'easeInExpo', 'easeInCirc', 'easeInBack', 'easeOutQuad', 'easeOutCubic', 'easeOutQuart', 'easeOutQuint', 'easeOutSine', 'easeOutExpo', 'easeOutCirc', 'easeOutBack', 'easeInOutQuad', 'easeInOutCubic', 'easeInOutQuart', 'easeInOutQuint', 'easeInOutSine', 'easeInOutExpo', 'easeInOutCirc', 'easeInOutBack']

function randomEasing() {
  let index = Math.floor(Math.random() * aminationEasings.length)
  return aminationEasings[index] }

// lodash radnom function
// @see: https://github.com/lodash/lodash/blob/master/random.js
function random(lower, upper, floating) {
  if (floating === undefined) {
    if (typeof upper == 'boolean') {
      floating = upper
      upper = undefined }
    else if (typeof lower == 'boolean') {
      floating = lower
      lower = undefined } }
  if (lower === undefined && upper === undefined) {
    lower = 0
    upper = 1 }
  else {
    if (upper === undefined) {
      upper = lower
      lower = 0 }}
  if (lower > upper) {
    const temp = lower
    lower = upper
    upper = temp }
  if (floating || lower % 1 || upper % 1) {
    const rand = Math.random()
    const randLength = `${rand}`.length - 1
    return Math.min(lower + (rand * (upper - lower + freeParseFloat(`1e-${randLength}`)), upper)) }
  return lower + Math.floor(Math.random() * (upper - lower + 1)) }

export default {
  addEvent,
  startAnimation,
  clearElement,
  guid,
  scrollTop,
  fontSize,
  isTouchDevice,
  getDevice,
  ready,
  randomEasing,
  random
}