import {AmbientLight,
        BackSide,
        Box3,
        BoxGeometry,
        BoxBufferGeometry,
        Clock,
        Color,
        FlatShading,
        Fog,
        ImageUtils,
        LinearFilter,
        LoadingManager,
        Math as ThreeMath,
        Mesh,
        OrthographicCamera,
        PerspectiveCamera,
        PlaneBufferGeometry,
        PlaneGeometry,
        RepeatWrapping,
        RGBFormat,
        RGBAFormat,
        Scene,
        ShaderMaterial,
        SmoothShading,
        Texture,
        TextureLoader,
        UniformsUtils,
        Vector2,
        Vector3,
        WebGLRenderer,
        WebGLRenderTarget }   from 'three'

import {BloomPass,
        ClearPass,
        DepthPass,
        DotScreenPass,
        EffectComposer, 
        FilmPass,
        GlitchPass,
        GodRaysPass,
        PixelationPass,
        RenderPass,
        SavePass,
        ShaderPass,
        SMAAPass,
        TexturePass}          from 'postprocessing'

import _                  from 'lodash'
import detector           from './lib/gl-detector'
import ShaderExtras       from './lib/shader-extras'
import { CopyShader }     from './lib/copy-shader'

const DEFAULT_OPTIONS = { fromAmplitude:  50,
                          toAmplitude:    0,
                          fromFrequency:  8,
                          toFrequency:    7,
                          fromOffset:    -30,
                          toOffset:       28,
                          autoplay:       false,
                          loop:           false,
                          duration:       8000 }

//  Thanks https://github.com/d3 for the plugin
function _contour(grid) {
  var s = [];
  var c = [];
  var x = s ? s[0] : 0;
  var y = s ? s[1] : 0;
  var dx = 0;
  var dy = 0;
  var pdx = NaN;
  var pdy = NaN;
  var i = 0;

  let f00 = 0

  while(true) {
    if(grid(x, y)) {
      s = [x, y];
      break;
    }
    if(x) {
      x = x - 1;
      y = y + 1;
    } else {
      x = y + 1;
      y = 0;
    }
  }

  do {
    i = 0;
    if(grid(x - 1, y - 1)) {
      i += 1;
    }
    if(grid(x, y - 1)) {
      i += 2;
    }
    if(grid(x - 1, y)) {
      i += 4;
    }
    if(grid(x, y)) {
      i += 8;
    }

    if(i === 6) {
      dx = pdy === -1 ? -1 : 1;
      dy = 0;
    } else
    if(i === 9) {
      dx = 0;
      dy = pdx === 1 ? -1 : 1;
    } else {
      dx = [1, 0, 1, 1, -1, 0, -1, 1, 0, 0, 0, 0, -1, 0, -1, NaN][i];
      dy = [0, -1, 0, 0, 0, -1, 0, 0, 1, -1, 1, 1, 0, -1, 0, NaN][i];
    }

    if(dx != pdx && dy != pdy) {
      c.push([x, y]);
      pdx = dx;
      pdy = dy;
    }

    x += dx;
    y += dy;
  } while(s[0] != x || s[1] != y)
    return c
}

function _loadTexture(τ) {
  return new Promise( (resolve, reject) => {
    new TextureLoader().load(τ, texture => resolve(texture)) }) }

function _transitionForProgressInRange(progress, startValue, endValue) {
  return startValue + (endValue - startValue) * progress }

function _progressForValueInRange(value, startValue, endValue) {
  return (value - startValue) / (endValue - startValue) }

function _clampedProgress(progress) {
  return Math.max(0, Math.min(progress, 1)) }

function _transitionInRange(startValue, endValue, startTime, endTime) {
    return _transitionForProgressInRange(_clampedProgress(_progressForValueInRange(progress, (startTime || 0) / options.duration, (endTime || options.duration) / options.duration)), startValue, endValue);
  };  

function _drawMaskImage(mask) {
  if(mask.img) {
    mask.canvas.width   = mask.img.width
    mask.canvas.height  = mask.img.height
    mask.ctx.drawImage(mask.img, 0, 0)
    let data = mask.ctx.getImageData(0, 0, mask.img.width, mask.img.height).data
    mask.points = _contour((x, y)  => data[(y * mask.img.width + x) * 4 + 3] > 0) }}

function _pathPoints(mask) {
    mask.ctx.beginPath()
    mask.points.forEach((point, index) => mask.ctx[index ? 'lineTo' : 'moveTo'](point[0], point[1]))
    mask.ctx.closePath() }

function _renderMask(mask) {
    if(!mask.points) return

    mask.ctx.clearRect(0, 0, mask.canvas.width, mask.canvas.height)
    mask.ctx.lineJoin   = 'round'
    mask.ctx.lineWidth  = Math.abs(mask.offset) * 2
    // mask.ctx.lineWidth  = 0

    if(_.isNumber(mask.offset)) {
      mask.ctx.globalCompositeOperation = 'source-' + (mask.offset < 0 ? 'out' : 'over')
      _pathPoints(mask)
      mask.ctx.stroke() }

    _pathPoints(mask)
    mask.ctx.fill() }

function _update(δ, mask, wasser) {
  
  // mask.offset += 0.2
  // if(mask.offset > 0 ) mask.offset = 0
  // else wasser.uniforms.Mask.value.needsUpdate = true

  wasser.uniforms.Amplitude.value -= 0.24
  if(wasser.uniforms.Amplitude.value < 0 ) wasser.uniforms.Amplitude.value = 0

  // wasser.uniforms.Frequency.value = wasser.uniforms.Frequency.value * 0.99
  // wasser.uniforms.Amplitude.value.needsUpdate = true

  // wasser.uniforms.Frequency.value = δ
  // mask.offset = δ


}

function _run(renderer, composition, mask, wasser) {
  // run…
  // ————————————————————————————
  let clock = new Clock(), δ,
      last  = performance.now(),
      now
  console.log('here we go —→')
  function _render() {
    // reschedule
    requestAnimationFrame(_render) 

    // timing
    now = performance.now()
    δ = (now - last) / 1000
    if (δ > 1) δ = 1 // safety cap on large deltas
    last = now

    _update(δ, mask, wasser)
    _renderMask(mask)
    renderer.clear()
    composition.render(δ)
  }
  // composition.render(0)
  _render() }

// Compose
// ————————————————————————————————
function _compose({renderer, scene, camera, wasser}) {
  let renderTarget    = new WebGLRenderTarget( window.innerWidth, window.innerHeight ),
      composition     = new EffectComposer( renderer, renderTarget ),
      clearPass       = new ClearPass({ clearColor: 0xffff00,
                                        clearAlphpa: 1 }),
      scenePass       = new RenderPass( scene, camera ),
      filmPass        = new FilmPass({greyscale:          false,
                                      sepia:              false,
                                      vignette:           false,
                                      eskil:              false,
                                      screenMode:         true,
                                      scanlines:          false,
                                      noise:              true,
                                      noiseIntensity:     0.5,
                                      scanlineIntensity:  0.5,
                                      scanlineDensity:    1.0,
                                      greyscaleIntensity: 1.0,
                                      sepiaIntensity:     1.0,
                                      vignetteOffset:     1.0,
                                      vignetteDarkness:   1.0 }),
      bloomPass       = new BloomPass({ resolutionScale: 0.5,
                                        intensity: 0.2 }),
      bleachShader    = { uniforms:       UniformsUtils.clone( ShaderExtras[ 'bleachbypass' ].uniforms ),
                          vertexShader:   ShaderExtras[ 'bleachbypass' ].vertexShader,
                          fragmentShader: ShaderExtras[ 'bleachbypass' ].fragmentShader},
      bleachMaterial  = new ShaderMaterial(bleachShader),
      bleachPass      = new ShaderPass( bleachMaterial ),
      wasserPass      = new ShaderPass( wasser )

  console.log('wasser', wasser)
  console.log('wasserPass', wasserPass)

  composition.addPass( clearPass )
  composition.addPass( wasserPass )
  // composition.addPass( bloomPass )
  // composition.addPass( filmPass )
  // composition.addPass( bleachPass )

  // clearPass.renderToScreen = true
  // scenePass.renderToScreen = true
  wasserPass.renderToScreen = true
  // bloomPass.renderToScreen = true
  // bleachPass.renderToScreen = true

  // turbulentPass.renderToScreen = true
  // outputPass.renderToScreen = true

  console.log('composition ready')
  return composition
}

function _initGL(_texture, _mask, options) {
  console.log('wasserfarben!', _texture, _mask, options) 
  options = _.defaults(options, DEFAULT_OPTIONS)

  // Scene
  // ————————————————————————————
  let scene   = new Scene,
      camera  = new OrthographicCamera(-1, 1, 1, -1, 0, 1),
      quad    = new Mesh(new PlaneBufferGeometry(2, 2))
  scene.add( quad )
  
  // Renderer
  // ————————————————————————————
  let renderer = new WebGLRenderer()
  document.getElementById('drei').appendChild(renderer.domElement)
  renderer.setPixelRatio( window.devicePixelRatio )
  renderer.setSize( window.innerWidth, window.innerHeight )
  renderer.setClearColor( 0xffffff )

  // Mask
  // ————————————————————————————
  let mask = {}
  mask.canvas = document.createElement('canvas')
  mask.ctx    = mask.canvas.getContext('2d')
  mask.offset = -30

  if(_.isString(_mask)) {
      mask.img         = new Image
      mask.img.onload  = () => _drawMaskImage(mask)
      mask.crossOrigin = 'Anonymous'
      mask.img.src     = _mask } 
  else if(_mask.nodeName && _mask.src) {
    mask.img = _mask
    _drawMaskImage(mask) }

  ImageUtils.crossOrigin = ''

  // Texture
  // ————————————————————————————  
  let τ = _texture && (_.isString(_texture) ? _texture : _texture.nodeName && _texture.src) || ''
  _loadTexture(τ)
    .then(texture => {
      
      texture.minFilter = LinearFilter

      let maskʈ = new Texture(mask.canvas)
      maskʈ.needsUpdate = true
      maskʈ.minFilter = LinearFilter

      let wasser𝐒 = { uniforms:       UniformsUtils.clone( ShaderExtras[ 'aquarelle' ].uniforms ),
                      vertexShader:   ShaderExtras[ 'aquarelle' ].vertexShader,
                      fragmentShader: ShaderExtras[ 'aquarelle' ].fragmentShader,
                      transparent:    true 
                    },
          wasser  = new ShaderMaterial(wasser𝐒)
      
      wasser.uniforms.Texture.value = texture
      wasser.uniforms.Mask.value = maskʈ
      wasser.uniforms.Amplitude.value = 160
      wasser.uniforms.Frequency.value = 4

      let composition = _compose({renderer, scene, camera, wasser})
      _run(renderer, composition, mask, wasser)

    })
}

function init(texture, mask, options) {
  // check that the browser is capable of handling WebGL
  if( !detector.webgl ) {
    detector.addGetWebGLMessage()
    console.log('no webgl.') 
    return }
  else _initGL(texture, mask, options)
}

export default { init }