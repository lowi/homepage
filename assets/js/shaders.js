import { Color,
         ShaderChunk,
         UniformsLib,
         UniformsUtils,
         Vector2,
         Vector3 } from 'three'

// the bird
let water = {
  uniforms: UniformsUtils.merge( [
              UniformsLib[ 'fog' ],
              UniformsLib[ 'lights' ],
              { 'time':             { value: 0},
                'delta':            { value: 0 },
                'texturePosition':  { value: null, type: 't' },
                'textureVelocity':  { value: null, type: 't' },
                'diffuse':          { value: new Color( 0xffffff) },
                'specular':         { value: new Color( 0xffffff) },
                'shininess':        { value: 30 }
              }]),
  
  vertexShader: [
  'attribute vec2 reference;',
  'attribute float birdVertex;',
  // 'attribute vec3 birdColor;',

  'uniform vec3 diffuse;',
  'uniform vec3 specular;',

  'uniform sampler2D texturePosition;',
  'uniform sampler2D textureVelocity;',
  'uniform float time;',

  'varying vec3 vNormal;',
  'varying float z;',
  'varying vec3 vViewPosition;',
  
  ShaderChunk[ 'shadowmap_pars_vertex' ],
  ShaderChunk[ 'fog_pars_vertex' ],
  
  'void main() {',
    // the w coordinate of tmpPos contains the phase (used for wing flapping)
    'vec4 tmpPos = texture2D( texturePosition, reference );',
    'vec3 pos = tmpPos.xyz;',
    'vec3 velocity = normalize(texture2D( textureVelocity, reference ).xyz);',
    'vec3 newPosition = position;',

    // flap the wings    
    // birdVertex is the index of the vertex within the bird gemoetry
    // index 4 and 7 are the outer tips of the wings
    'if ( birdVertex == 4.0 || birdVertex == 7.0 ) {',
      'newPosition.y = sin( tmpPos.w ) * 5.; }',

    // questionable
    'vViewPosition = -mat3( modelViewMatrix ) * newPosition;',

    'newPosition  = mat3( modelMatrix ) * newPosition;',
    'vec3 norm    = mat3( modelMatrix ) * vec3(0.0, 1.0, 0.0);',
    
    'velocity.z   *= -1.;',
    'float xz     = length( velocity.xz );',
    'float xyz    = 1.;',
    'float x      = sqrt( 1. - velocity.y * velocity.y );',
    'float cosry  = velocity.x / xz;',
    'float sinry  = velocity.z / xz;',
    'float cosrz  = x / xyz;',
    'float sinrz  = velocity.y / xyz;',
    'mat3 maty =  mat3(',
      'cosry, 0, -sinry,',
      '0    , 1, 0     ,',
      'sinry, 0, cosry',
    ');',
    'mat3 matz =  mat3(',
      'cosrz , sinrz, 0,',
      '-sinrz, cosrz, 0,',
      '0     , 0    , 1',
    ');',
    'newPosition =  maty * matz * newPosition;',
    'newPosition += pos;',
    
    'z = newPosition.z;',
    'vNormal = maty * matz * norm;',

    'gl_Position = projectionMatrix *  viewMatrix  * vec4( newPosition, 1.0 );',
  '}'].join( "\n" ),

  fragmentShader: [
  'varying float z;',
  'varying vec3 vViewPosition;',
  'varying vec3 vNormal;',

  'uniform vec3 diffuse;',
  'uniform vec3 specular;',
  'uniform float shininess;',

  ShaderChunk[ 'common' ],
  ShaderChunk[ 'bsdfs' ],
  ShaderChunk[ 'lights_pars' ],
  ShaderChunk[ 'shadowmap_pars_fragment' ],
  ShaderChunk[ 'fog_pars_fragment' ],

  "float calcLightAttenuation( float lightDistance, float cutoffDistance, float decayExponent ) {",
    "if ( decayExponent > 0.0 ) {",
      "return pow( saturate( - lightDistance / cutoffDistance + 1.0 ), decayExponent );",
    "}",
    "return 1.0;",
  "}",

  'void main() {',
    // outgoing light does not have an alpha, the surface does
    'vec3 outgoingLight = vec3( 0.0 );',
    'vec4 diffuseColor  = vec4(diffuse, 1.0);',
    'vec3 viewPosition  = normalize( vViewPosition );',

    "vec3 normal = normalize( vNormal );",
    'vec3 totalDiffuseLight  = vec3( 0.0 );',
    'vec3 totalSpecularLight = vec3( 0.0 );',

    // point lights
    // "#if NUM_POINT_LIGHTS > 0",
    //   "for ( int i = 0; i < NUM_POINT_LIGHTS; i ++ ) {",
    //     "vec3 lVector = pointLights[ i ].position + vViewPosition.xyz;",
    //     "float attenuation = calcLightAttenuation( length( lVector ), pointLights[ i ].distance, pointLights[ i ].decay );",
    //     "lVector = normalize( lVector );",
    //     "float pointDiffuseWeight = max( dot( normal, lVector ), 0.0 );",
    //     "totalDiffuseLight += attenuation * pointLights[ i ].color * pointDiffuseWeight;",

    //     "vec3 pointHalfVector = normalize( lVector + viewPosition );",
    //     "float pointDotNormalHalf = max( dot( normal, pointHalfVector ), 0.0 );",
    //     "float pointSpecularWeight = specularTex.r * max( pow( pointDotNormalHalf, shininess ), 0.0 );",
    //     "totalSpecularLight += attenuation * pointLights[ i ].color * specular * pointSpecularWeight * pointDiffuseWeight;",
    //   "}",
    // "#endif",

    // directional lights
    "#if NUM_DIR_LIGHTS > 0",
      "vec3 dirDiffuse = vec3( 0.0 );",
      "vec3 dirSpecular = vec3( 0.0 );",
      "for( int i = 0; i < NUM_DIR_LIGHTS; i++ ) {",
        "vec3 dirVector = directionalLights[ i ].direction;",
        "float dirDiffuseWeight = max( dot( normal, dirVector ), 0.0 );",

        "vec3 dirHalfVector = normalize( dirVector + viewPosition );",
        "float dirDotNormalHalf = max( dot( normal, dirHalfVector ), 0.0 );",
        "totalDiffuseLight += directionalLights[ i ].color * dirDiffuseWeight;",
        "float dirSpecularWeight =  max( pow( dirDotNormalHalf, shininess ), 0.0 );",
        "totalSpecularLight += directionalLights[ i ].color * specular * dirSpecularWeight * dirDiffuseWeight;",
        // "totalSpecularLight += 1.0;",
      "}",

    "#endif",

    // 'outgoingLight += vec3( 1.0, 1.0, 1.0 ) * ( totalDiffuseLight + totalSpecularLight );',
    'outgoingLight += diffuse * ( totalDiffuseLight + totalSpecularLight );',
    'gl_FragColor = vec4( outgoingLight, 1.0 );',
    ShaderChunk[ 'fog_fragment' ], 

  '}'].join( "\n" )
}


export default { water }