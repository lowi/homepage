import {DataTexture,
        RGBAFormat }  from 'three'

// create a plain data texture with the given dimensions vec2 δ
function makeDataTexture(δ){
  let dataMap = new Uint8Array(1 << (Math.floor(Math.log(δ * δ * 4) / Math.log(2)))),

      // DataTexture( data, width, height, format, type, mapping, wrapS, wrapT, magFilter, minFilter, anisotropy )
      texture = new DataTexture(dataMap, δ, δ, RGBAFormat)

  // initialize the data map all black and totally transparent
  _(dataMap)
    .chunk(4)                 // split into pixel chunks
    .each((δ, ι) => { 
      dataMap[ι*4]    = 0     // r
      dataMap[ι*4+1]  = 0     // g
      dataMap[ι*4+2]  = 0     // b
      dataMap[ι*4+3]  = 0 })  // α
    
  texture.needsUpdate = true
  return texture }

export default { makeDataTexture }