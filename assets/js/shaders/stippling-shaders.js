import {Color, Vector2, Vector3, Vector4} from 'three'

function defaultVertexShader() {
  return ['varying vec2 vUv; ',
          'void main() {',
              'vUv = uv;',
              'vec4 mvPosition = modelViewMatrix * vec4(position, 1.0 );',
              'gl_Position = projectionMatrix * mvPosition; }' ].join('\n') }


// Return true if `a` and `b` are at most EPSILON apart
// in any dimension
function approxEqual() {
  return ['bool approxEqual(const vec4 a, const vec4 b) {',
              'return all(lessThan(abs(a - b), vec4(EPSILON)));}',

          'bool approxEqual(const vec2 a, const vec2 b) {',
              'return all(lessThan(abs(a - b), vec2(EPSILON)));}'
          ].join('\n') }

function validUv() {
  return ['bool between(const vec2 value, const vec2 bottom, const vec2 top) {',
              'return (',
                  'all(greaterThan(value, bottom)) &&',
                  'all(lessThan(value, top))); }',
          'bool validUv(const vec2 uv) {',
              'return between(',
                  'uv,',
                  'vec2(0., 0.),',
                  'vec2(1., 1.) );}'].join('\n') }

function compare() {
  return ['vec4 compare(const vec4 self, const vec2 offset) {',
            'vec2 sampleUv = vUv + offset;',
            'vec4 sample = texture2D(tInput, sampleUv);',
            
            // if the sampleUv is out of bounds: invalidate
            'if (!validUv(sampleUv)) { sample = createInvalidCell(); }',
            
            // Other is invalid. This means that 'offset' is off the grid or
            // 'otherCell' doesn't have any seed info yet.
            'if (!isValidCell(sample)) { return self; }',
            
            // // Our seed location hasn't been set but other's has.
            'else if (!isValidCell(self)) { return sample; }',
            
            'else { ',
              'float selfDist = distance(self.xy, vUv);',
              'float sampleDist = distance(sample.xy, vUv);',
              'if (selfDist > sampleDist) { return sample; }',
            ' }',

            'return self; }',
].join('\n') }
  


// Split a float into two base-255 encoded floats. Useful for storing
// a screen co-ordinate as the rg part or ba part of a pixel.
// This can be passed fractional values. If it's passed (300.5, 300.5)
// then it will return
//     vec2(floor(300.5 / 255.), mod(300.5, 255.))
//
// which is vec2(1.0, 45.5). Then later, when decoding, we return
//     channels.x * 255. + channels.y
//
// which is 1.0 * 255. + 45.5 which is 300.5. The returned pair has
// values in the interval [0.0, 255.0). This means that it can be
// stored as a color value in an UNSIGNED_BYTE texture.
//
// function encodeData() {
//   return ['vec2 encodeData(const float value_) {',
//             'float value = value_;',
//             'return vec2( floor(value / 100.), mod(value, 100.) );',
//             '}'].join('\n') }

// function decodeData() {
//   return ['float decodeData(const vec2 channels) {',
//             'return channels.x * 100. + channels.y;', 
//             '}'].join('\n') }

function isValidCell() {
  return ['bool isValidCell(const vec4 data) {',
            'return data.z == 0.; }'].join('\n') }

function createCell() {
  return ['vec4 createCell(const vec2 uv_) {',
            'return vec4(uv_, 0., 1.);',
            '}'].join('\n') }

function createInvalidCell() {
  return ['vec4 createInvalidCell() {',
          'return vec4(0., 0., 1., 0.);}'].join('\n') }


function luma() {
  return ['float luma(vec3 color) {',
            'return dot(color, vec3(0.299, 0.587, 0.114));}',
          'float luma(vec4 color) {',
            'return dot(color.rgb, vec3(0.299, 0.587, 0.114));}' ].join('\n') }


let StipplingShaders = {

  'prepareJFA': {
    uniforms: {
      iBackgroundColor: { type: 'v4', value: new Vector4( 0.0, 0.0, 0.0, 0.0 ) },
      iResolution:      { type: 'f',  value: 0 },
      tSeeds:           { type: 't',  value: 0, texture: null } },

    vertexShader: defaultVertexShader(),
      
    fragmentShader: [
      'varying vec2 vUv;',
      'uniform vec4 iBackgroundColor;',
      'uniform float iResolution;',
      'uniform sampler2D tSeeds;',

      // 0.005 * 255 is roughly 1.2, so this will match colors
      // one digit away from each other.
      'const float EPSILON = 0.005;',

      approxEqual(),

      // Each pixel in our grid texture is a cell object. Each cell contains
      // the following info (-1.0, seedIndex, locationX, locationY). The
      // following functions are an 'object-oriented' set of functions for
      // handling cells.
      ////////////////////////////////////////////////////////////////////////
      createCell(),
      createInvalidCell(),
      luma(),

      'void main() {',
        'vec4 color = texture2D(tSeeds, vUv);',
        'float brightness = luma(color);',
        // 'vec2 vUvFlippedY = vec2(vUv.x, 1.0 - vUv);',
        // 'vec4 color = texture2D(tSeeds, vUvFlippedY);',
        'if (approxEqual(color, iBackgroundColor)) {',
          'gl_FragColor = createInvalidCell();',
        '} else {',
          // we found a seed color
          'gl_FragColor = createCell(vUv);',
          // 'gl_FragColor = vec4(vec3(brightness), 1.0);',

        '}',
      '}' ].join('\n') },

  'JFA': {
    uniforms: {
      iStepSize:    { type: 'f', value: 0 },
      iResolution:  { type: 'f', value: 0 },
      tInput:       { type: 't', value: 0, texture: null }},
    vertexShader: defaultVertexShader(),
    fragmentShader: [

      'varying vec2 vUv;',
      'uniform float iStepSize;',
      'uniform float iResolution;',
      'uniform sampler2D tInput;',

      createCell(),
      createInvalidCell(),
      validUv(),
      isValidCell(),
      compare(),

      'void main() {',
        'vec4 thiz = texture2D(tInput, vUv);',

        // check in all directions
        // ↖ ↑ ↗
        // ← ◌ →
        // ↙ ↓ ↘
        'for (int y = -1; y <= 1; ++y) {',
          'for (int x = -1; x <= 1; ++x) {',
            'vec2 offset = vec2(x, y) * (iStepSize/iResolution);',
            'thiz = compare(thiz, offset);',
            '}}',
        'gl_FragColor = thiz;',
      '}'
    ].join('\n') },

    'voronoi': {
      uniforms: {
        tInput:       { type: 't', value: 0, texture: null },
        tSeeds:       { type: 't', value: 0, texture: null },
        iResolution:  { type: 'f', value: 0 }},

      vertexShader: defaultVertexShader(),

      fragmentShader: [

        'varying vec2 vUv;',
        'uniform int iStepSize;',
        'uniform float iResolution;',
        'uniform sampler2D tInput;',
        'uniform sampler2D tSeeds;',
        
        isValidCell(),

        'void main() {',
          'vec4 data  = texture2D(tInput, vUv);',
          'vec4 color = texture2D(tSeeds, data.xy);',
          // 'gl_FragColor = data;',

          'if (isValidCell(data)) {',
            'gl_FragColor = color;',
            '} ',
          'else {',
            'gl_FragColor = vec4(0); }',

        '}' ].join('\n') }

}

export default StipplingShaders