const webpack           = require('webpack')
const Clean             = require('clean-webpack-plugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const extractSass       = new ExtractTextPlugin('index.css')

module.exports = {
  entry: {
    index: __dirname + '/assets/js/index.js'},
    
  output: {
    path: __dirname + '/public/assets',
    filename: '[name].js',
    publicPath: '/assets' },
    
  module: {
    loaders: [

      { test: /.*\.sass$/,
        loader: extractSass.extract(['css-loader', 'sass-loader', 'import-glob-loader'])},
      
      { test: /\.js$/,
        // exclude: /(node_modules)/,
        include: [ __dirname + 'assets/js', /\/node_modules\/postprocessing/],
        loader: 'babel-loader',
        query: { presets: ['es2015'] } },

      { test:     /\.glslx$/,
        exclude:  /(node_modules)/,
        loader:   'glslx-loader' },

      { test:   /\.json$/,
        loader: 'json-loader' },

      // images
      { test: /\.(jpe?g|png|gif|svg)$/i, loader: "file-loader?name=/img/[name].[ext]"},

      // { test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,     loader: 'url-loader?limit=65000&mimetype=image/svg+xml&name=/fonts/[name].[ext]' },
      { test: /\.woff(\?v=\d+\.\d+\.\d+)?$/,    loader: 'url-loader?limit=65000&mimetype=application/font-woff&name=/fonts/[name].[ext]' },
      { test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/,   loader: 'url-loader?limit=65000&mimetype=application/font-woff2&name=/fonts/[name].[ext]' },
      { test: /\.[ot]tf(\?v=\d+\.\d+\.\d+)?$/,  loader: 'url-loader?limit=65000&mimetype=application/octet-stream&name=/fonts/[name].[ext]' },
      { test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,     loader: 'url-loader?limit=65000&mimetype=application/vnd.ms-fontobject&name=/fonts/[name].[ext]'}
    ]
  },
  plugins: [
    extractSass,
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify(process.env.NODE_ENV),
      }
    }),
  ],
  devServer: {
    port:     3000,
    inline:   true,
    stats:    'minimal'
  }
};

