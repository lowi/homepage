---
title: About
images:
  - 'https://www.datocms-assets.com/3197/1504777797-team.jpeg?auto=compress&w=800'
  - >-
    https://www.datocms-assets.com/3197/1504872027-tumblr_ovwz7fyjhz1qz6f9yo1_1280.jpg?auto=compress&w=800
  - >-
    https://www.datocms-assets.com/3197/1504872027-tumblr_ovwz7fyjhz1qz6f9yo2_1280.jpg?auto=compress&w=800
content:
  - text: >-
      # Wir sind Studio KNACK.  


      Wir gestalten für Menschen und Marken.  


      Architektur, Innenarchitektur,  Ausstellungsdesign,
      Kommunikationsdesign.  

      Ganzheitliche Konzepte. Maßgeschneiderte Lösungen.  

      Innovatives Design. Hohe Qualität.  
  - teammember:
      image:
        url: >-
          https://www.datocms-assets.com/3197/1504869810-andi.jpg?auto=compress&w=800
        ratio: 1
      name: Andi
      role: partner
      description: |-
        Oh Du, Geliebte meiner 27 Sinne, ich liebe Dir!
        Du, Deiner, Dich Dir, ich Dir, Du mir, ---- wir?
        Das gehört beiläufig nicht hierher!
        Wer bist Du, ungezähltes Frauenzimmer, Du bist, bist Du?
        Die Leute sagen, Du wärest.
        Laß sie sagen, sie wissen nicht, wie der Kirchturm steht.
        Du trägst den Hut auf Deinen Füßen und wanderst auf die Hände,
        Auf den Händen wanderst Du.
        Halloh, Deine roten Kleider, in weiße Falten zersägt,
        Rot liebe ich Anna Blume, rot liebe ich Dir.
        Du, Deiner, Dich Dir, ich Dir, Du mir, ----- wir?
        Das gehört beiläufig in die kalte Glut!
        Anna Blume, rote Anna Blume, wie sagen die Leute?
  - text: >
      ## Interactivity — Who Cares?


      This paper places contemporary modalities of digital interaction in an
      historical context of a quarter century of technological development and
      artistic experimentation. 2011.


      ----


      ## Art After Computing


      This paper follows three related strands of techno-cultural development
      related to the interactions between computing and arts practices over the
      last quarter century. The first is aesthetico-theoretical in nature, is
      recognition of the radically new kinds of cultural practices made possible
      by real time computing (especially interactive practices) and the
      complementary recognition that new modes of performative and relational
      aesthetics are called for. The second strand concerns the tacit or covert
      incursion of ideologies of computing into arts practices and the
      possibility that such ideologies may have had the pernicious effect of
      devaluing or disenfranchising, or simply rendering invisible or
      irrelevant, traditional practices and values which themselves may have
      been inadequately or poorly explicated. The third strand tracks the
      collapse of the cognitivist Cartesian worldview around which ʻgood old
      fashioned artificial intelligenceʼ (GOFAI) and cognitive science were
      framed, and the emergence of situated, embodied, enactive and distributed
      paradigms of cognition. 2010.


      Techno–utopianism, Embodied Interaction and the Aesthetics of Behavior —
      an interview with Simon Penny


      This text is an edited version of an interview conducted by Jihoon Felix
      Kim at the International Symposium on Art and Technology, Korea National
      University of the Arts, Seoul, Korea, November 2008. Transcribed by
      Kristen Galvin, edited by Kristen Galvin, and Simon Penny. 2008-2010.
  - teammember:
      image:
        url: >-
          https://www.datocms-assets.com/3197/1504871161-simon.jpg?auto=compress&w=800
        ratio: 1
      name: Simon
      role: partner
      description: >-
        Unaussprechliche Namen können schützen: Der Radfahrer heißt zum Beispiel
        Wrdlbrmpfd. Dem Polizisten ist das zu kompliziert für einen Strafzettel.
        Bei anderen Valentinschen Namens-Neuschöpfungen wird sofort klar, dass
        die Figur lächerlich ist, wie Herr Rembremerdeng oder Mister
        Hamtnquempftn. Valentins Erfindungsreichtum braucht sich hier nicht vor
        dem des irischen Schriftstellers James Joyce zu verstecken.
  - teammember:
      image:
        url: >-
          https://www.datocms-assets.com/3197/1504871527-cat.gif?auto=compress&w=800
        ratio: 0.9920634920634921
      name: Julian
      role: partner
      description: 'Mögen hätt ich schon wollen, aber dürfen hab ich mich nicht getraut.'
seoMetaTags: >-
  <title>About — Studio Knack</title><meta property="og:title"
  content="About"><meta name="twitter:title" content="About"><meta
  name="description" content="A holistic design studio based in lovely
  Munich."><meta property="og:description" content="A holistic design studio
  based in lovely Munich."><meta name="twitter:description" content="A holistic
  design studio based in lovely Munich."><meta name="twitter:card"
  content="summary"><meta property="article:modified_time"
  content="2017-09-20T14:09:38Z"><meta property="og:locale"
  content="en_EN"><meta property="og:type" content="article"><meta
  property="og:site_name" content="studio knack"><meta property="og:image"
  content="https://www.datocms-assets.com/3197/1506430106-logo.png?"><meta
  name="twitter:image"
  content="https://www.datocms-assets.com/3197/1506430106-logo.png?">
type: extra
layout: about
---

